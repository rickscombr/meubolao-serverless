# APP - MEU BOLAO
<div align="left"><img src="./src/assets/imgs/logo-circle.png" width="200"/></div>


## Tecnologias Utilizadas

* Node
* Yarn
* Express
* Axios
* MongoDB
* Mongoose
* Uuid
* Jwt
* ESlint
* Prettier
* Plop
* Yup


## Pré-requisitos

* [Git](https://git-scm.com/)
* [Node](http://nodejs.org/)
* [Yarn](https://yarnpkg.com/)


## Respositório na núvem

* [GitLab](https://gitlab.com/rickscombr/meubolao-serverless.git)


## Clone do Projeto

```sh
$ git clone git@gitlab.com:rickscombr/meubolao-serverless.git
```

## Instalação

```sh
$ yarn
```

## Executando Local (DEV)

```sh
$ yarn dev
```

## Executando Produção (PRD)

```sh
$ yarn start
```

## Deploy usando
Atualmente utilizando VPS KingHost
Acesso através do terminal

```sh
# connect to server
$ ssh root@XXX.XXX.XXX.XXX
$ <PASSWORD>

# change the project folder
$ cd <PATH>/<PROJECT>
# update project version by Master branch
$ git pull
$ <GIT_USER>
$ <GIT_PASSWORD>
# install dependencies project
$ yarn

# Se necessário atualize as variáveis de ambiente no servidor
$ sudo nano /etc/environment
# Cole o export das variáveis necessárias
export NODE_ENV="prd"
export WEB_URL="https://meubolao.net"
export V3_APIFOOTBALL_URL="XXX...."
export V3_APIFOOTBALL_KEY="XXX...."
# ...
# Salve o arquivo e desconecte do servidor
```

Acesse o Painel KingHost e vá no Console Web
- Reinicie o Servidor
- Faça novo login
- Navege até a pasta do projeto
- Cheque se as variáveis estão carregadas

```sh
$ echo $NODE_ENV
# deve ter o retorno (prd)

# inicie o projeto
$ yarn start
```

# Integrações com API Football
Verificar a cocumentação da API em: [API Football v3](https://www.api-football.com/documentation-v3)

Configurar as variáveis de ambiente:
- V3_APIFOOTBALL_URL
- V3_APIFOOTBALL_KEY
