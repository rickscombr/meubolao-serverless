exports.ContactGetTemplateService = async ({ id, name, phone, email, message }) => {
    const emailBody = `
        <!DOCTYPE html>
        <html lang="pt-br">
        	<head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                <link rel="preconnect" href="https://fonts.googleapis.com">
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600&display=swap" rel="stylesheet">
        	</head>
            <body bgcolor="#262743" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
                <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#262743" height="100%" style="border: none; background-color: #262743; height: 100%;">
                    <tr>
                        <td>
                        <div align="center">
                            <table border="0" width="600" cellspacing="0" cellpadding="0" style="width: 600px;">
                                <tr>
                                    <td>
                                    <img border="0" src="https://meubolao.net/img/email/header.png" width="600" height="145"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#F8CB00" style="background-color: #F8CB00;">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="30" height="80">&nbsp;</td>
                                            <td width="540" height="80">
                                                <p style="text-align: center; font-family: Montserrat; font-size: 40px; font-weight: 300;">Contato</p>
                                            </td>
                                            <td width="30" height="80">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="30">&nbsp;</td>
                                            <td width="540">
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Olá <strong>${name}</strong>,</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Recebemos sua mensagem enviada através do formulário de Fale Conosco da plataforma <strong>MeuBolão</strong> com as seguintes informações:</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;"></p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Nome: <strong>${name}</strong></p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">E-Mail: <strong>${email}</strong></p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Celular: <strong>${phone}</strong></p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 600;">Dados da Mensagem:</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">${message}</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;"></p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Muito em breve retornaremos seu contato através do seu e-mail: ${email}</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Desde já agradecemos sua mensagem.</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 400;">Obrigado.</p>
                                                <p style="font-family: Montserrat; font-size: 18px; font-weight: 600;">Equipe Meubolão</p>
                                            </td>
                                            <td width="30">&nbsp;</td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <img border="0" src="https://meubolao.net/img/email/footer.png" width="600" height="50"></td>
                                </tr>
                            </table>
                        </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    `
    return emailBody
}
