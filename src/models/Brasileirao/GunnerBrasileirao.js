const { Schema, connection } = require('mongoose')

const gunnerSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    seasonYear: {
        type: Number,
        required: true
    },
    teamId: {
        type: Number,
        required: true
    },
    teamName: {
        type: String,
        required: true
    },
    teamLogo: {
        type: String,
        required: true
    },
    playerId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: false
    },
    nationality: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: true
    },
    games: {
        type: Number,
        required: true
    },
    shots: {
        type: Number,
        required: true
    },
    goals: {
        type: Number,
        required: true
    },
    goalsConceded: {
        type: Number,
        required: true
    },
    goalsAssists: {
        type: Number,
        required: true
    },
    goalsSaves: {
        type: Number,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const GunnerBrasileirao = Brasileirao.model("Gunner", gunnerSchema)

module.exports = GunnerBrasileirao
