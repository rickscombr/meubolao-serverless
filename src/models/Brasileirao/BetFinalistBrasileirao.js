const { Schema, connection } = require('mongoose')

const betFinalistSchema = new Schema({
    pollId: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    place1: {
        type: Number,
        required: true
    },
    place2: {
        type: Number,
        required: true
    },
    place3: {
        type: Number,
        required: true
    },
    place17: {
        type: Number,
        required: true
    },
    place18: {
        type: Number,
        required: true
    },
    place19: {
        type: Number,
        required: true
    },
    place20: {
        type: Number,
        required: true
    },
    gunner: {
        type: Number,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const BetFinalistBrasileirao = Brasileirao.model("BetFinalist", betFinalistSchema)

module.exports = BetFinalistBrasileirao
