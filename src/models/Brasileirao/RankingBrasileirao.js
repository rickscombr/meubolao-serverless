const { Schema, connection } = require('mongoose')

const rankingSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    seasonYear: {
        type: Number,
        required: true
    },
    teamId: {
        type: Number,
        required: true
    },
    rank: {
        type: Number,
        required: true
    },
    group: {
        type: String,
        required: true
    },
    lastRank: {
        type: String,
        required: true
    },
    points: {
        type: Number,
        required: true
    },
    games: {
        type: Number,
        required: true
    },
    win: {
        type: Number,
        required: true
    },
    draw: {
        type: Number,
        required: true
    },
    lose: {
        type: Number,
        required: true
    },
    goalsFor: {
        type: Number,
        required: true
    },
    goalsAgainst: {
        type: Number,
        required: true
    },
    goalsDiff: {
        type: Number,
        required: true
    },
    tag: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const RankingBrasileirao = Brasileirao.model("Ranking", rankingSchema)

module.exports = RankingBrasileirao
