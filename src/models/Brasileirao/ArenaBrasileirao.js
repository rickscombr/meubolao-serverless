const { Schema, connection } = require('mongoose')

const arenaSchema = new Schema({
    arenaId: {
        type: Number,
        required: true
    },
    teamId: {
        type: Number,
        required: true
    },
    countryId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    display: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    capacity: {
        type: Number,
        required: false
    },
    image: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const ArenaBrasileirao = Brasileirao.model("Arena", arenaSchema)

module.exports = ArenaBrasileirao
