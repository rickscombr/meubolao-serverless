const { Schema, connection } = require('mongoose')

const participantSchema = new Schema({
    pollId: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    pointPlace1: {
        type: Number,
        required: true
    },
    pointPlace2: {
        type: Number,
        required: true
    },
    pointPlace3: {
        type: Number,
        required: true
    },
    pointPlace17: {
        type: Number,
        required: true
    },
    pointPlace18: {
        type: Number,
        required: true
    },
    pointPlace19: {
        type: Number,
        required: true
    },
    pointPlace20: {
        type: Number,
        required: true
    },
    pointGunner: {
        type: Number,
        required: true
    },
    pointGames: {
        type: Number,
        required: true
    },
    pointTotal: {
        type: Number,
        required: true
    },
    rank: {
        type: Number,
        required: true
    },
    rankOld: {
        type: Number,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const ParticipantBrasileirao = Brasileirao.model("Participant", participantSchema)

module.exports = ParticipantBrasileirao
