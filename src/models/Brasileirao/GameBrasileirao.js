const { Schema, connection } = require('mongoose')

const gameSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    seasonYear: {
        type: Number,
        required: true
    },
    gameId: {
        type: Number,
        required: true
    },
    roundId: {
        type: String,
        required: true
    },
    masterId: {
        type: Number,
        required: true
    },
    visitorId: {
        type: Number,
        required: true
    },
    arenaId: {
        type: Number,
        required: true
    },
    date: {
        type: Number,
        required: true
    },
    timezone: {
        type: String,
        required: true
    },
    masterGoals: {
        type: Number,
        required: true
    },
    visitorGoals: {
        type: Number,
        required: true
    },
    winner: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})
const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const GameBrasileirao = Brasileirao.model("Game", gameSchema)

module.exports = GameBrasileirao
