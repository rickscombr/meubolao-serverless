const { Schema, connection } = require('mongoose')

const awardSchema = new Schema({
    pollId: {
        type: Number,
        required: true
    },
    positionStart: {
        type: Number,
        required: true
    },
    positionEnd: {
        type: Number,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Brasileirao = connection.useDb(process.env.MONGO_DATABASE_BRASILEIRAO)
const AwardBrasileirao = Brasileirao.model("Award", awardSchema)

module.exports = AwardBrasileirao
