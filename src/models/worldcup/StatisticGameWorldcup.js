const { Schema, connection } = require('mongoose')

const statisticGameSchema = new Schema({
    gameId: {
        type: Number,
        required: true
    },
    teamId: {
        type: Number,
        required: true
    },
    shotsGoal: {
        type: Number,
        required: true
    },
    shots: {
        type: Number,
        required: true
    },
    shotsInsideBox: {
        type: Number,
        required: true
    },
    shotsOutsideBox: {
        type: Number,
        required: true
    },
    fouls: {
        type: Number,
        required: true
    },
    cornerKicks: {
        type: Number,
        required: true
    },
    offsides: {
        type: Number,
        required: true
    },
    ballPossession: {
        type: String,
        required: true
    },
    YellowCards: {
        type: Number,
        required: true
    },
    RedCards: {
        type: Number,
        required: true
    },
    goalkeeperSaves: {
        type: Number,
        required: true
    },
    passesTotal: {
        type: Number,
        required: true
    },
    passesAccurate: {
        type: Number,
        required: true
    },
    passesPercent: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Worldcup = connection.useDb(process.env.MONGO_DATABASE_WORLDCUP)
const StatisticGameWorldcup = Worldcup.model("StatisticGame", statisticGameSchema)

module.exports = StatisticGameWorldcup
