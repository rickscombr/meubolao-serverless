const { Schema, connection } = require('mongoose')

const playerSchema = new Schema({
    playerId: {
        type: Number,
        required: true
    },
    teamId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: false
    },
    number: {
        type: Number,
        required: false
    },
    position: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Worldcup = connection.useDb(process.env.MONGO_DATABASE_WORLDCUP)
const PlayerWorldcup = Worldcup.model("Player", playerSchema)

module.exports = PlayerWorldcup
