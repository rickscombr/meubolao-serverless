const { Schema, connection } = require('mongoose')

const roundSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    seasonYear: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    originalName: {
        type: String,
        required: true
    },
    round: {
        type: Number,
        required: true
    },
    current: {
        type: Boolean,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Worldcup = connection.useDb(process.env.MONGO_DATABASE_WORLDCUP)
const RoundWorldcup = Worldcup.model("Round", roundSchema)

module.exports = RoundWorldcup
