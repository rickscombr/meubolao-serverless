const { Schema, connection } = require('mongoose')

const gameSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    seasonYear: {
        type: Number,
        required: true
    },
    gameId: {
        type: Number,
        required: true
    },
    roundId: {
        type: String,
        required: true
    },
    masterId: {
        type: Number,
        required: true
    },
    visitorId: {
        type: Number,
        required: true
    },
    arenaName: {
        type: String,
        required: false
    },
    arenaCity: {
        type: String,
        required: false
    },
    date: {
        type: Number,
        required: true
    },
    timezone: {
        type: String,
        required: true
    },
    masterGoals: {
        type: Number,
        required: true
    },
    visitorGoals: {
        type: Number,
        required: true
    },
    winner: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})
const Worldcup = connection.useDb(process.env.MONGO_DATABASE_WORLDCUP)
const GameWorldcup = Worldcup.model("Game", gameSchema)

module.exports = GameWorldcup
