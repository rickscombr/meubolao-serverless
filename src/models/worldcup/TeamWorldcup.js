const { Schema, connection } = require('mongoose')

const teamSchema = new Schema({
    teamId: {
        type: Number,
        required: true
    },
    countryId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    founded: {
        type: Number,
        required: false
    },
    national: {
        type: Boolean,
        required: true
    },
    logo: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Worldcup = connection.useDb(process.env.MONGO_DATABASE_WORLDCUP)
const TeamWorldcup = Worldcup.model("Team", teamSchema)

module.exports = TeamWorldcup
