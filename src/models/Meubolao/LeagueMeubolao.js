const { Schema, connection } = require('mongoose')

const leagueSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    countryId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    display: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Meubolao = connection.useDb(process.env.MONGO_DATABASE_MEUBOLAO)
const LeagueMeubolao = Meubolao.model("League", leagueSchema)

module.exports = LeagueMeubolao
