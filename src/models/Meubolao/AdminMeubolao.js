const { Schema, connection } = require('mongoose')

const adminSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    cpf: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        require: false
    },
    password: {
        type: String,
        required: true
    },
    polls: {
        type: Array,
        required: true
    },
    codeVerify: {
        type: String,
        require: false
    },
    codeExpires: {
        type: Number,
        require: false
    },
    verified: {
        type: Boolean,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Meubolao = connection.useDb(process.env.MONGO_DATABASE_MEUBOLAO)
const AdminMeubolao = Meubolao.model("Admin", adminSchema)

module.exports = AdminMeubolao
