const { Schema, connection } = require('mongoose')

const userSchema = new Schema({
    cpf: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        required: false
    },
    heartTeam: {
        type: String,
        required: false
    },
    verifyCode: {
        type: String,
        required: false
    },
    status: {
        type: String,
        enum: ['active', 'inactive', 'pending', 'removed'],
        default: 'pending',
        required: true
    },
}, {
    timestamps: true
})

const Meubolao = connection.useDb(process.env.MONGO_DATABASE_MEUBOLAO)
const UserMeubolao = Meubolao.model("User", userSchema)

module.exports = UserMeubolao
