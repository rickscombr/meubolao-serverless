const { Schema, connection } = require('mongoose')

const countrySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    code: {
        type: String,
    },
    flag: {
        type: String,
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Meubolao = connection.useDb(process.env.MONGO_DATABASE_MEUBOLAO)
const CountryMeubolao = Meubolao.model("Country", countrySchema)

module.exports = CountryMeubolao
