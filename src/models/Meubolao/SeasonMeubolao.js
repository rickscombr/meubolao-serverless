const { Schema, connection } = require('mongoose')

const seasonSchema = new Schema({
    leagueId: {
        type: Number,
        required: true
    },
    year: {
        type: Number,
        required: true
    },
    start: {
        type: Number,
        required: true
    },
    end: {
        type: Number,
        required: true
    },
    current: {
        type: Boolean,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
}, {
    timestamps: true
})

const Meubolao = connection.useDb(process.env.MONGO_DATABASE_MEUBOLAO)
const SeasonMeubolao = Meubolao.model("Season", seasonSchema)

module.exports = SeasonMeubolao
