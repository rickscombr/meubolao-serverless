const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { AdmAuthenticateService } = require('../../../../services/admin/meubolao/authenticateService')
const { isEmail } = require('../../../../services/utils/validate')

exports.handler = async (event, context) => {
    await MongoConnection()

    const { email, password: usePassword } = JSON.parse(event.body)
    if (!isEmail(email)) {
        return Response('400-006')
    }
    if (!usePassword) {
        return Response('400-008')
    }
    try {
        const login = await AdmAuthenticateService({ email, usePassword })
        const {
            logged,
            token,
            id,
            name,
            email: useEmail
        } = login
        if (logged) {
            const userInfo = {
                id,
                name,
                email: useEmail,
                logged,
                token
            }
            return Response('200-018', userInfo)
        } else {
            return Response('401-000')
        }
    } catch (error) {
        return Response('500-000', error)
    } finally {
        await MongoDisconnection()
    }
}
