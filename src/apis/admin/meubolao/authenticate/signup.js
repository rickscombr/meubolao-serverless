const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { AmdCreateService, AdmFindEmailService } = require('../../../../services/admin/meubolao/authenticateService')
const { isName, isCpf, isCelular, isEmail } = require('../../../../services/utils/validate')

exports.handler = async (event, context) => {
    try {
        const { name, cpf, email, phone, password } = JSON.parse(event.body)
        if (!isCpf(cpf)) {
            return Response('400-003')
        }
        if (!isCelular(phone)) {
            return Response('400-005')
        }
        if (!isEmail(email)) {
            return Response('400-006')
        }
        if (!isName(name)) {
            return Response('400-007')
        }

        try {
            await MongoConnection()
            try {
                const user = await AdmFindEmailService({ email })
                if (user.length > 0) {
                    return Response('409-000')
                }
                try {
                    const data = await AmdCreateService({
                        name,
                        document: cpf,
                        email,
                        phone,
                        password,
                    })
                    return Response('201-010', data)
                } catch (error) {
                    return Response('500-043', error)
                }

            } catch (error) {
                return Response('500-046', error)
            }
        } catch (error) {
            return Response('500-001', error)
        }
    } catch (error) {
        return Response('400-000', error.message)
    } finally {
        await MongoDisconnection()
    }
}
