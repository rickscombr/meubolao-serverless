const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootLeagueService } = require('../../../../services/admin/footballService')
const { CountryCreateService } = require('../../../../services/admin/meubolao/countryService')
const { LeagueCreateService } = require('../../../../services/admin/meubolao/leagueService')
const { SeasonCreateService } = require('../../../../services/admin/meubolao/seasonService')

exports.handler = async (event, context) => {
    try {
        const { leagueId, year, display } = JSON.parse(event.body)
        await MongoConnection()
        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os dados da league / Country / Season
            const resp = await FootLeagueService({ leagueId, year })
            const { league, country, seasons } = resp[0]
            const season = seasons[0]
            try {
                //PASSO 2: Cadastra / Atualiza o Country e retorna o ID
                const { id: countryId } = await CountryCreateService({ country })
                try {
                    //PASSO 3: Cadastra / Atualiza a League e a retorna
                    const newLeague = await LeagueCreateService({ countryId, league, display })
                    try {
                        //PASSO 4: Cadastra / Atualiza a Season e a retorna
                        const newSeason = await SeasonCreateService({ leagueId, season })

                        const data = {
                            league: newLeague,
                            country: {
                                id: countryId,
                                name: country.name,
                                code: country.code,
                                flag: country.flag
                            },
                            season: newSeason
                        }
                        return Response('201-001', data)

                    } catch (error) {
                        return Response('500-015', error)
                    }
                } catch (error) {
                    return Response('500-007', error)
                }
            } catch (error) {
                return Response('500-011', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-001', error)
    } finally {
        await MongoDisconnection()
    }
}
