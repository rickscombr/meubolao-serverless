const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { LeagueFindService } = require('../../../../services/admin/meubolao/leagueService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId } = event.pathParameters
        const response = await LeagueFindService({ leagueId })
        return Response('200-002', response)
    } catch (error) {
        return Response('500-006', error)
    } finally {
        await MongoDisconnection()
    }
}
