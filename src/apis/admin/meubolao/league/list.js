const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { LeagueListService } = require('../../../../services/admin/meubolao/leagueService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const response = await LeagueListService()
        return Response('200-001', response)
    } catch (error) {
        return Response('500-005', error)
    } finally {
        await MongoDisconnection()
    }
}
