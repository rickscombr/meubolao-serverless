const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { CountryFindService } = require('../../../../services/admin/meubolao/countryService')

exports.handler = async (event, context) => {
    await MongoConnection()
    const { id } = event.pathParameters
    try {
        const response = await CountryFindService({ id })
        return Response('200-004', response)
    } catch (error) {
        return Response('500-010', error)
    } finally {
        await MongoDisconnection()
    }
}
