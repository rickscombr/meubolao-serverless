const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { CountryListService } = require('../../../../services/admin/meubolao/countryService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const response = await CountryListService()
        return Response('200-003', response)
    } catch (error) {
        return Response('500-009', error)
    } finally {
        await MongoDisconnection()
    }
}
