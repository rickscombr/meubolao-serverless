const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { SeasonListService } = require('../../../../services/admin/meubolao/seasonService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const response = await SeasonListService()
        return Response('200-005', response)
    } catch (error) {
        return Response('500-013', error)
    } finally {
        await MongoDisconnection()
    }
}
