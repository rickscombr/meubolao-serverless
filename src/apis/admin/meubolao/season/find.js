const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { SeasonFindService } = require('../../../../services/admin/meubolao/seasonService')

exports.handler = async (event, context) => {
    await MongoConnection()
    const { year } = event.pathParameters
    try {
        const response = await SeasonFindService({ year })
        return Response('200-006', response)
    } catch (error) {
        return Response('500-014', error)
    } finally {
        await MongoDisconnection()
    }
}
