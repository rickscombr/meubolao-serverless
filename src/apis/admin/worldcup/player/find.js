const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { PlayerFindService } = require('../../../../services/admin/worldcup/playerService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { playerId } = event.pathParameters
        const response = await PlayerFindService({ playerId })
        return Response('200-010', response)
    } catch (error) {
        return Response('500-022', error)
    } finally {
        await MongoDisconnection()
    }
}
