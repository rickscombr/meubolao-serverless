const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootPlayerService } = require('../../../../services/admin/footballService')
const { PlayersRemoveService, PlayerCreateService } = require('../../../../services/admin/worldcup/playerService')

exports.handler = async (event, context) => {
    try {
        const { teamId } = JSON.parse(event.body)
        await MongoConnection()
        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os dados de Teams e Arenas
            const data = await FootPlayerService({ teamId })
            try {
                //PASSO 2: Apaga todos os jogadores antes de recadastrar
                await PlayersRemoveService({ teamId })
                try {
                    //PASSO 3: Cadastra os jogadores do time
                    const response = await PlayerCreateService({ data })
                    return Response('201-005', response)
                } catch (error) {
                    return Response('500-023', error)
                }
            } catch (error) {
                return Response('500-024', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
