const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { RoundListService } = require('../../../../services/admin/worldcup/roundService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await RoundListService({ leagueId, seasonYear })
        return Response('200-013', response)
    } catch (error) {
        return Response('500-029', error)
    } finally {
        await MongoDisconnection()
    }
}
