const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { RoundFindService } = require('../../../../services/admin/worldcup/roundService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await RoundFindService({ leagueId, seasonYear })
        return Response('200-014', response)
    } catch (error) {
        return Response('500-030', error)
    } finally {
        await MongoDisconnection()
    }
}
