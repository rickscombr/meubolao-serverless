const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { GameListService } = require('../../../../services/admin/worldcup/gameService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await GameListService({ leagueId, seasonYear })
        return Response('200-015', response)
    } catch (error) {
        return Response('500-034', error)
    } finally {
        await MongoDisconnection()
    }
}
