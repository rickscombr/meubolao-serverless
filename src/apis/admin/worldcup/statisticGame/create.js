const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootStatisticGameService } = require('../../../../services/admin/footballService')
const { StatisticGameCreateService } = require('../../../../services/admin/worldcup/statisticGameService')

exports.handler = async (event, context) => {
    try {
        await MongoConnection()
        const { gameId } = JSON.parse(event.body)

        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os jogos do campeonato
            const game = await FootStatisticGameService({ gameId })
            try {
                //PASSO 2: Cadastra / Atualiza as estatística da partida
                const statisticGameCreated = await StatisticGameCreateService({ gameId, game })
                return Response('201-017', statisticGameCreated)
            } catch (error) {
                return Response('500-040', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
