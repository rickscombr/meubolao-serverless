const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { StatisticGameFindService } = require('../../../../services/admin/worldcup/statisticGameService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { gameId } = event.pathParameters
        const response = await StatisticGameFindService({ gameId })
        return Response('200-017', response)
    } catch (error) {
        return Response('500-039', error)
    } finally {
        await MongoDisconnection()
    }
}
