const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootGunnerService } = require('../../../../services/admin/footballService')
const { GunnerCreateService } = require('../../../../services/admin/brasileirao/gunnerService')

exports.handler = async (event, context) => {
    console.log('EVENT BODY ===>', event.body)
    try {
        await MongoConnection()
        const { leagueId, seasonYear } = JSON.parse(event.body)
        console.log('LEAGUE ID ===>', leagueId)
        console.log('SEASON YEAR ===>', seasonYear)
        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os jogos do campeonato
            console.log('PASSO 1.1 ==>', leagueId)
            console.log('PASSO 1.2 ==>', seasonYear)
            const gunners = await FootGunnerService({ leagueId, seasonYear })
            console.log('PASSO 2 ==>', gunners)

            try {
                //PASSO 2: Cadastra / Atualiza os jogos do campeonato
                const gunnersCreated = await GunnerCreateService({ leagueId, seasonYear, gunners })
                console.log('PASSO 3 ==>', gunnersCreated)
                return Response('201-011', gunnersCreated)
            } catch (error) {
                return Response('500-050', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
