const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { GunnerListService } = require('../../../../services/admin/brasileirao/gunnerService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await GunnerListService({ leagueId, seasonYear })
        return Response('200-022', response)
    } catch (error) {
        return Response('500-049', error)
    } finally {
        await MongoDisconnection()
    }
}
