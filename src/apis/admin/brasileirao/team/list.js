const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { TeamListService } = require('../../../../services/admin/brasileirao/teamService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const response = await TeamListService()
        return Response('200-007', response)
    } catch (error) {
        return Response('500-017', error)
    } finally {
        await MongoDisconnection()
    }
}
