const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { TeamFindService } = require('../../../../services/admin/brasileirao/teamService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { teamId } = event.pathParameters
        const response = await TeamFindService({ teamId })
        return Response('200-008', response)
    } catch (error) {
        return Response('500-014', error)
    } finally {
        await MongoDisconnection()
    }
}
