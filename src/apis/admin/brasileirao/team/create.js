const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootTeamService } = require('../../../../services/admin/footballService')
const { LeagueFindService } = require('../../../../services/admin/meubolao/leagueService')
const { TeamCreateService } = require('../../../../services/admin/brasileirao/teamService')

exports.handler = async (event, context) => {
    try {
        const { leagueId, year } = JSON.parse(event.body)
        if (!leagueId) {
            return Response('400-001')
        }
        if (!year) {
            return Response('400-002')
        }
        await MongoConnection()
        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os dados de Teams e Arenas
            const data = await FootTeamService({ leagueId, year })
            console.log('PASSO 1 ===================>', data)
            try {
                //PASSO 2: Recupera o countryId da League
                const { countryId } = await LeagueFindService({ leagueId })
                console.log('PASSO 2 ===================>', countryId)
                try {
                    //PASSO 3: Cadastra / Atualiza os times da league
                    const response = await TeamCreateService({ data, countryId })
                    console.log('PASSO 3 ===================>', response)
                    return Response('201-004', response)
                } catch (error) {
                    return Response('500-019', error)
                }
            } catch (error) {
                return Response('500-006', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
