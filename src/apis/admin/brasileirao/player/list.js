const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { PlayerListService } = require('../../../../services/admin/brasileirao/playerService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { teamId } = event.pathParameters
        const response = await PlayerListService({ teamId })
        return Response('200-009', response)
    } catch (error) {
        return Response('500-021', error)
    } finally {
        await MongoDisconnection()
    }
}
