const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootGameService } = require('../../../../services/admin/footballService')
const { GameCreateService } = require('../../../../services/admin/brasileirao/gameService')

exports.handler = async (event, context) => {
    try {
        await MongoConnection()
        const { leagueId, seasonYear, round } = JSON.parse(event.body)

        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os jogos do campeonato
            const games = await FootGameService({ leagueId, seasonYear, round })
            try {
                //PASSO 2: Cadastra / Atualiza os jogos do campeonato
                const gamesCreated = await GameCreateService({ leagueId, seasonYear, games })
                return Response('201-008', gamesCreated)
            } catch (error) {
                return Response('500-036', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
