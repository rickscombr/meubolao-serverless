const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { GameFindService } = require('../../../../services/admin/brasileirao/gameService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { gameId } = event.pathParameters
        const response = await GameFindService({ gameId })
        return Response('200-016', response)
    } catch (error) {
        return Response('500-035', error)
    } finally {
        await MongoDisconnection()
    }
}
