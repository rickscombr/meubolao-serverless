const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootRankingService } = require('../../../../services/admin/footballService')
const { RankingRemoveService, RankingCreateService } = require('../../../../services/admin/brasileirao/rankingService')

exports.handler = async (event, context) => {
    try {
        await MongoConnection()
        const { leagueId, seasonYear } = JSON.parse(event.body)
        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar os dados da classificação da league na temporada
            const data = await FootRankingService({ leagueId, seasonYear })
            try {
                //PASSO 2: Apaga os dados da classificação atual
                const clear = await RankingRemoveService({ leagueId, seasonYear })
                try {
                    //PASSO 3: Cadastra / Atualiza a Classificação da Temporada
                    const ranking = await RankingCreateService({ leagueId, seasonYear, data })
                    return Response('201-006', ranking)
                } catch (error) {
                    return Response('500-027', error)
                }
            } catch (error) {
                return Response('500-028', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
