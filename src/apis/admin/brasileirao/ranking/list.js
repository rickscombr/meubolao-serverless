const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { RankingListService } = require('../../../../services/admin/brasileirao/rankingService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await RankingListService({ leagueId, seasonYear })

        return Response('200-011', response)
    } catch (error) {
        return Response('500-025', error)
    } finally {
        await MongoDisconnection()
    }
}
