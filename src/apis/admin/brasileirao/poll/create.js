const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { PollFindService, PollCreateService } = require('../../../../services/admin/brasileirao/pollService')

exports.handler = async (event, context) => {
    try {
        const { leagueId, seasonYear, key, name, start, end, current } = JSON.parse(event.body)
        const active = true

        await MongoConnection()
        try {
            //PASSO 1: Verifica se existe o mesmo bolão já cadastrado
            const data = await PollFindService({ leagueId, seasonYear, key })
            if (data) {
                return Response('409-000')
            }
            try {
                //PASSO 3: Cadastra os jogadores do time
                const response = await PollCreateService({ leagueId, seasonYear, key, name, start, end, current, active })
                return Response('201-012', response)
            } catch (error) {
                return Response('500-051', error)
            }
        } catch (error) {
            return Response('500-052', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
