const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { PollFindService } = require('../../../../services/admin/brasileirao/pollService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear, pollKey: key } = event.pathParameters
        const response = await PollFindService({ leagueId, seasonYear, key })
        if (!response) {
            return Response('404-002')
        }
        return Response('200-024', response)
    } catch (error) {
        return Response('500-052', error)
    } finally {
        await MongoDisconnection()
    }
}
