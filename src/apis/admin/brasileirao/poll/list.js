const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { PollListService } = require('../../../../services/admin/brasileirao/pollService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear } = event.pathParameters
        const response = await PollListService({ leagueId, seasonYear })
        return Response('200-023', response)
    } catch (error) {
        return Response('500-053', error)
    } finally {
        await MongoDisconnection()
    }
}
