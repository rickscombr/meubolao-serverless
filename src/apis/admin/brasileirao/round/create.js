const { Response } = require('../../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../../services/utils/connection')
const { FootRoundService } = require('../../../../services/admin/footballService')
const { RoundListService, RoundCreateService, RoundUpdateService } = require('../../../../services/admin/brasileirao/roundService')

exports.handler = async (event, context) => {
    try {
        await MongoConnection()
        const { leagueId, seasonYear } = JSON.parse(event.body)

        try {
            //PASSO 1: Chamar a API de Footbal v3 para buscar a rodada atual do campeonato
            const actual = true
            const current = await FootRoundService({ leagueId, seasonYear, actual })
            try {
                //PASSO 2: Saber se já existem rodadas cadastradas
                const roundsExist = await RoundListService({ leagueId, seasonYear })
                if (roundsExist.length <= 0) {
                    // Ainda não cadastradas as rodadas, cadastrar
                    try {
                        //PASSO 3.1: Chamar a API de Footbal v3 para buscar as rodadas do campeonato
                        const rounds = await FootRoundService({ leagueId, seasonYear })
                        try {
                            //PASSO 4.1: Cadastrar as rodadas do campeonato e atualizar a rodada atual
                            const roundsCreated = await RoundCreateService({ leagueId, seasonYear, rounds, current })
                            return Response('201-007', roundsCreated)
                        } catch (error) {
                            return Response('500-031', error)
                        }
                    } catch (error) {
                        return Response('500-004', error)
                    }
                } else {
                    // Rodadas já cadastradas, atualizar
                    try {
                        //PASSO 3.2: Atualiza a rodada atual do campeonado
                        const roundsUpdates = await RoundUpdateService({ leagueId, seasonYear, current })
                        return Response('202-013', roundsUpdates)
                    } catch (error) {
                        return Response('500-032', error)
                    }
                }
            } catch (error) {
                return Response('500-029', error)
            }
        } catch (error) {
            return Response('500-004', error)
        }
    } catch (error) {
        return Response('500-002', error)
    } finally {
        await MongoDisconnection()
    }
}
