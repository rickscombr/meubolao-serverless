const handler = require('.')

describe('src/api/version/index', () => {
    test('Should return project version info', () => {
        const { body } = handler()
        const { meta, data } = JSON.parse(body)
        expect(data.name).toEqual('meubolao-serverless')
        expect(meta.status).toBe(200)
    });
});
