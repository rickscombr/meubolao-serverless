const { Response } = require('../../services/utils/responses')
const {
    name,
    version,
    description,
    keywords,
    repository,
    author,
    license,
} = require('../../../package.json')

exports.handler = (event, context) => {
    const response = {
        name,
        version,
        description,
        keywords,
        repository,
        author,
        license,
    }

    return Response('200-000', response)
}
