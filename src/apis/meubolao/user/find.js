const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { UserFindService } = require('../../../services/meubolao/userService')

exports.handler = async (event, context) => {
    await MongoConnection()
    const { id } = event.pathParameters
    try {
        const response = await UserFindService({ _id: id })
        if (!response) {
            return Response('404-001', response)
        }
        return Response('200-020', response)
    } catch (error) {
        return Response('500-047', error)
    } finally {
        await MongoDisconnection()
    }
}
