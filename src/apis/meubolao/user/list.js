const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { UserListService } = require('../../../services/meubolao/userService')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const response = await UserListService()
        if (!response && response.length < 1) {
            return Response('200-404')
        }
        return Response('200-019', response)
    } catch (error) {
        return Response('500-046', error)
    } finally {
        await MongoDisconnection()
    }
}
