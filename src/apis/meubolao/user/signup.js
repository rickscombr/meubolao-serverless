const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { UserFindService, UserCreateService } = require('../../../services/meubolao/userService')
const { ClaroIsClientService } = require('../../../services/partner/claroService')
const { isName, isCpf, isCelular, isEmail } = require('../../../services/utils/validate')

exports.handler = async (event, context) => {
    try {
        const { cpf, phone, name, email, password, heartTeam } = JSON.parse(event.body)
        if (!isCpf(cpf)) {
            return Response('400-003')
        }
        if (!isCelular(phone)) {
            return Response('400-005')
        }
        if (!isEmail(email)) {
            return Response('400-006')
        }
        if (!isName(name)) {
            return Response('400-007')
        }

        try {
            await MongoConnection()
            try {
                const user = await UserFindService({ cpf })
                if (user) {
                    return Response('409-000')
                }
                try {
                    const { validate } = await ClaroIsClientService({ cpf })
                    if (!validate) {
                        return Response('500-000', { message: 'Invalid partner customer' })
                    }
                    try {
                        const data = await UserCreateService({
                            cpf,
                            phone,
                            name,
                            email,
                            password,
                            heartTeam
                        })
                        return Response('201-010', data)
                    } catch (error) {
                        return Response('500-043', error)
                    }

                } catch (error) {
                    return Response('500-045', error)
                }
            } catch (error) {
                return Response('500-046', error)
            }
        } catch (error) {
            return Response('500-001', error)
        }
    } catch (error) {
        return Response('400-000', error.message)
    } finally {
        await MongoDisconnection()
    }
}
