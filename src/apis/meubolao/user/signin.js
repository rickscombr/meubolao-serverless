const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { UserSigninService, UserTokenSigninService } = require('../../../services/meubolao/userService')
const { isCpf } = require('../../../services/utils/validate')

exports.handler = async (event, context) => {
    await MongoConnection()
    try {
        const { user: useUser, password: usePassword } = JSON.parse(event.body)
        if (!isCpf(useUser)) {
            return Response('400-003')
        }
        if (!usePassword) {
            return Response('400-008')
        }
        const login = await UserSigninService({ useUser, usePassword })
        const { logged } = login
        if (logged) {
            return Response('200-018', login)
        } else {
            return Response('401-000')
        }
    } catch (error) {
        return Response('500-006', error)
    } finally {
        await MongoDisconnection()
    }
}

exports.token = async (event, context) => {
    await MongoConnection()
    try {
        const { token } = JSON.parse(event.body)
        const login = await UserTokenSigninService({ token })
        const { logged } = login
        if (logged) {
            return Response('200-018', login)
        } else {
            return Response('401-000')
        }
    } catch (error) {
        return Response('500-006', error)
    } finally {
        await MongoDisconnection()
    }
}
