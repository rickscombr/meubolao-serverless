const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { UserFindService, UserUpdateService } = require('../../../services/meubolao/userService')
const { isName, isCelular, isEmail } = require('../../../services/utils/validate')

exports.handler = async (event, context) => {
    try {
        const { id } = event.pathParameters
        const { name, phone, email, password, team } = JSON.parse(event.body)
        if (phone) {
            if (!isCelular(phone)) {
                return Response('400-005')
            }
        }
        if (email) {
            if (!isEmail(email)) {
                return Response('400-006')
            }
        }
        if (name) {
            if (!isName(name)) {
                return Response('400-007')
            }
        }
        try {
            await MongoConnection()
            try {
                const user = await UserFindService({ _id: id })
                if (!user) {
                    return Response('404-001')
                }
                try {
                    const data = await UserUpdateService({
                        id,
                        name,
                        phone,
                        email,
                        password,
                        heartTeam: team
                    })
                    return Response('200-021')
                } catch (error) {
                    return Response('500-048', error)
                }
            } catch (error) {
                return Response('500-047', error)
            }
        } catch (error) {
            return Response('500-001', error)
        }
    } catch (error) {
        return Response('400-000', error.message)
    } finally {
        await MongoDisconnection()
    }
}
