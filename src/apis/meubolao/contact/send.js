const { Response } = require('../../../services/utils/responses')
const { MailSendService } = require('../../../services/utils/sendMail')
const { MailGetTemplateService } = require('../../../assets/emails/contact')
const { ContactGetTemplateService } = require('../../../assets/emails/contactSended')
const { isName, isCelular, isEmail } = require('../../../services/utils/validate')

exports.handler = async (event, context) => {
    let useEmail
    let emailBody
    const emailSubject = `Brasileirão - Fale Conosco`

    try {
        const { id, name, phone, email, message } = JSON.parse(event.body)
        if (phone) {
            if (!isCelular(phone)) {
                return Response('400-005')
            }
        }
        if (email) {
            if (!isEmail(email)) {
                return Response('400-006')
            }
        }
        if (name) {
            if (!isName(name)) {
                return Response('400-007')
            }
        }

        try {
            // Envia e-Mail para a conta do MeuBolão
            useEmail = process.env.EMAIL_CONTACT
            emailBody = await MailGetTemplateService({ id, name, phone, email, message })
            await MailSendService({ useEmail, emailBody, emailSubject })

            // Envia e-Mail para para o usuário
            useEmail = email
            emailBody = await ContactGetTemplateService({ id, name, phone, email, message })
            await MailSendService({ useEmail, emailBody, emailSubject })

            return Response('200-054')
        } catch (error) {
            return Response('500-055')
        }
    } catch (error) {
        return Response('400-000', error.message)
    }
}
