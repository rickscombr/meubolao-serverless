const { Response } = require('../../../services/utils/responses')
const { MongoConnection, MongoDisconnection } = require('../../../services/utils/connection')
const { GameLastNextService, GameListService } = require('../../../services/brasileirao/gameService')

exports.lastGames = async (event, context) => {
    await MongoConnection()
    try {
        const { limit } = event.pathParameters
        const status = "FT" // Finished Time
        const order = -1    // Last games
        const response = await GameLastNextService({ status, order, limit })
        return Response('200-015', response)
    } catch (error) {
        return Response('500-034', error)
    } finally {
        await MongoDisconnection()
    }
}
exports.nextGames = async (event, context) => {
    await MongoConnection()
    try {
        const { limit } = event.pathParameters
        const status = "NS" // Not Started
        const order = 1     // Next games
        const response = await GameLastNextService({ status, order, limit })
        return Response('200-015', response)
    } catch (error) {
        return Response('500-034', error)
    } finally {
        await MongoDisconnection()
    }
}
exports.listGames = async (event, context) => {
    await MongoConnection()
    try {
        const { leagueId, seasonYear, roundId } = event.pathParameters
        const response = await GameListService({ leagueId, seasonYear, roundId })
        return Response('200-015', response)
    } catch (error) {
        return Response('500-034', error)
    } finally {
        await MongoDisconnection()
    }
}
