const UserMeubolao = require('../../models/meubolao/UserMeubolao')
const {
    GenerateVerifyCode,
    EncryptPassword,
    ValidatePassword,
    TokenJwt,
    TokenJwtDecode
} = require('../utils/password')
const { Today } = require('../utils/dates')
const { formatCpf, formatCelular } = require('../utils/validate')

exports.UserListService = async () => {
    const users = await UserMeubolao.find()
    return users
}

exports.UserFindService = async (data) => {
    const user = await UserMeubolao.find(data)
    if (user.length > 0) {
        return user[0]
    } else {
        return null
    }
}

exports.UserCreateService = async ({ cpf, phone, name, email, password, heartTeam }) => {
    const document = formatCpf(cpf)
    const celular = formatCelular(phone)
    const verifyCode = await GenerateVerifyCode()
    const encryptedPassword = await EncryptPassword({ password })
    const avatar = 'https://meubolao.net/apps/claro/user/avatar.png'
    const status = 'pending'
    let user

    const oldUser = await this.UserFindService({ cpf })
    if (oldUser) {
        user = null
    } else {
        user = await UserMeubolao.create({
            cpf: document,
            phone: celular,
            name,
            email,
            password: encryptedPassword,
            avatar,
            heartTeam,
            verifyCode,
            status,
        })
    }
    const response = {
        id: user.id,
        name: user.name,
        cpf: user.cpf,
        createdAt: user.createdAt,
    }
    return response
}

exports.UserSigninService = async ({ useUser, usePassword }) => {
    let response = {
        logged: false,
        token: null,
        id: null,
        name: null,
        email: null,
        phone: null,
        avatar: null
    }
    const cpf = await formatCpf(useUser)
    try {
        const user = await this.UserFindService({ cpf })
        if (!!user.id) {
            const { id, name, email, phone, password, avatar } = user
            const valida = await ValidatePassword({ usePassword, password })
            if (valida) {
                const token = await TokenJwt({ id, name, email, phone, avatar })
                if (!!token) {
                    response = {
                        logged: true,
                        token,
                        id,
                        name,
                        email,
                        phone,
                        avatar
                    }
                }
            }
        }
        return response
    } catch (error) {
        return error
    }
}

exports.UserTokenSigninService = async ({ token }) => {
    let response = {
        logged: false,
        token: null,
        id: null,
        name: null,
        email: null,
        phone: null,
        avatar: null
    }
    const { id, name, email, phone, avatar, exp: expires, iat: created } = await TokenJwtDecode({ token })
    if (!!id && !!name) {
        const { timestamp } = Today('utc')
        if (expires > timestamp) {
            //Token valido
            response = {
                logged: true,
                token,
                id,
                name,
                email,
                phone,
                avatar
            }
        }
    }
    return response
}

exports.UserUpdateService = async ({ id, name, phone, email, password, heartTeam }) => {
    let user, celular, encryptedPassword
    if (phone) {
        celular = formatCelular(phone)
    }
    if (password) {
        encryptedPassword = await EncryptPassword({ password })
    }

    const dataUpdate = {
        name,
        phone: celular,
        email,
        password: encryptedPassword,
        heartTeam
    }
    const oldUser = await this.UserFindService({ _id: id })
    if (oldUser) {
        user = await UserMeubolao.findByIdAndUpdate({ _id: id }, dataUpdate)
    } else {
        user = null
    }
    return user
}
