const axios = require('axios')

exports.ClaroIsClientService = async ({ cpf }) => {
    // const urlClaro = `https://${process.env.CLARO_HOST}/isClient?cpf=${cpf}`
    // const options = {
    //     headers: {
    //         'x-rapidapi-key': `${process.env.CLARO_API_KEY}`,
    //         'x-rapidapi-host': `${process.env.CLARO_HOST}`
    //     }
    // }
    // const { data: { response } } = await axios.get(urlClaro, options)
    if (!cpf) {
        return {
            validate: false
        }
    }
    const response = {
        validate: true
    }
    return response
}
