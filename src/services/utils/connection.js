const mongoose = require('mongoose')
const { Response } = require('./responses')

exports.MongoConnection = async () => {
    const MONGO_USER = process.env.MONGO_USER
    const MONGO_PASSWORD = process.env.MONGO_PASSWORD
    const MONGO_HOST = process.env.MONGO_HOST
    const MONGO_PORT = process.env.MONGO_PORT
    const MONGO_DATABASE_MEUBOLAO = process.env.MONGO_DATABASE_MEUBOLAO
    const MONGO_DATABASE_BRASILEIRAO = process.env.MONGO_DATABASE_BRASILEIRAO
    const MONGO_DATABASE_WORLDCUP = process.env.MONGO_DATABASE_WORLDCUP
    const MONGO_STR = process.env.MONGO_STR
    const MONGO_OPTIONS = process.env.MONGO_OPTIONS
    let connStr = `${MONGO_STR}://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}`
    if (MONGO_PORT > 0) {
        connStr = `${connStr}:${MONGO_PORT}`
    }
    if (MONGO_OPTIONS) {
        connStr = `${connStr}/?${MONGO_OPTIONS}`
    }

    try {
        await mongoose.connect(connStr, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            dbName: MONGO_DATABASE_MEUBOLAO,
        })
        try {
            await mongoose.connect(connStr, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                dbName: MONGO_DATABASE_BRASILEIRAO,
            })
            try {
                await mongoose.connect(connStr, {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    dbName: MONGO_DATABASE_WORLDCUP,
                })
                console.log('Conection databases successfuly !')
                return true
            } catch (error) {
                console.log(`Conection databases error [${MONGO_DATABASE_WORLDCUP}] !`)
                await Response('500-002', error)
            }
        } catch (error) {
            console.log(`Conection databases error [${MONGO_DATABASE_BRASILEIRAO}] !`)
            await Response('500-002', error)
        }
    } catch (error) {
        console.log(`Conection databases error [${MONGO_DATABASE_MEUBOLAO}] !`)
        await Response('500-001', error)
    }
}

exports.MongoDisconnection = async () => {
    try {
        await mongoose.disconnect()
        return true
    } catch (error) {
        await Response('500-003')
    }
}
