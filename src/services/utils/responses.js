const messages = require('../../assets/json/messages.json')
const { version } = require('../../../package.json')

const { Today  } = require('./dates')

exports.Response = async (code, data) => {
    const { dateTime } = await Today('utc')
    try {
        const status = parseInt(code.substring(0,3))
        const type = parseInt(code.substring(0,1))
        let prefix

        if(type === 1){
            prefix = "WAN-"
        } else if(type === 2){
            prefix = "SUC-"
        } else if(type === 4){
            prefix = "ERR-"
        } else {
            //type = 5
            prefix = "ERR-"
        }
        const response = {
            meta: {
                status,
                code: `${prefix}${code}`,
                msg: messages[code],
                date: dateTime,
            },
            data
        }
        return {
            statusCode: status,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'x-api-version': version,
            },
            body: JSON.stringify(response),
        }
    } catch(err){
        response = {
            meta: {
                status: 500,
                code: '500-000',
                msg: err,
                date: dateTime,
            }
        }
        return {
            statusCode: 500,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'x-api-version': version,
            },
            body: JSON.stringify(response),
        }
    }
}
