const jwt = require('jsonwebtoken')
const { Today } = require('./dates')
const { Response } = require('./responses')

const generatePolicy = async (principalId, effect, resource, data) => {
    try {
        let authResponse = {}
        authResponse.principalId = principalId
        if (effect && resource) {
            let policyDocument = {}
            policyDocument.Version = '2012-10-17'
            policyDocument.Statement = []
            let statementOne = {}
            statementOne.Action = 'execute-api:Invoke'
            statementOne.Effect = effect
            statementOne.Resource = resource
            policyDocument.Statement[0] = statementOne
            authResponse.policyDocument = policyDocument
        }
        authResponse.context = data
        return authResponse
    } catch (error) {
        return error
    }
}

exports.authorizeAuth = async (event, context) => {
    const AUTH_KEY = process.env.JWT_AUTH_KEY
    const authorizerArr = event.authorizationToken.split(' ')
    const token = authorizerArr[1]
    const methodArn = event.methodArn
    const { timestamp } = await Today('utc')
    try {
        if (authorizerArr[0] !== 'Bearer' || !token) {
            await Response('401-000')
        }
        const data = await jwt.verify(token, AUTH_KEY)
        const { id, iat, exp } = data

        if (timestamp >= iat && timestamp <= exp) {
            const response = await generatePolicy(id, "Allow", methodArn, data)
            return response
        } else {
            await Response('401-000')
        }
    } catch (error) {
        await Response('401-000', error)
    }
}
exports.authorizeAdmin = async (event, context) => {
    const ADM_KEY = process.env.JWT_ADM_KEY
    const authorizerArr = event.authorizationToken.split(' ')
    const token = authorizerArr[1]
    const methodArn = event.methodArn
    const { timestamp } = await Today('utc')
    try {
        if (authorizerArr[0] !== 'Bearer' || !token) {
            await Response('401-000')
        }

        const data = await jwt.verify(token, ADM_KEY)
        const { id, iat, exp } = data

        if (timestamp >= iat && timestamp <= exp) {
            const response = await generatePolicy(id, "Allow", methodArn, data)
            return response
        } else {
            await Response('401-000')
        }
    } catch (error) {
        await Response('401-000')
    }
}
