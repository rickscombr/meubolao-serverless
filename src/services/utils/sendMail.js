const nodemailer = require("nodemailer");
const { Response } = require('./responses')

exports.MailSendService = async ({ useEmail, emailBody, emailSubject }) => {
    const emailConfig = {
        pool: false,
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure: process.env.EMAIL_SECT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS,
        },
    }

    try {
        const transporter = nodemailer.createTransport(emailConfig)
        const mailOption = {
            from: `${process.env.EMAIL_USER}`,
            to: useEmail,
            subject: emailSubject,
            html: emailBody,
        };
        const info = await transporter.sendMail(mailOption)
        return info

    } catch (error) {
        return Response('500-055')
    }
}
