const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

exports.EncryptPassword = async ({ password }) => {
    try {
        const newPassword = await bcrypt.hash(password, 10)
        return newPassword
    } catch (error) {
        return error
    }
}
exports.ValidatePassword = async ({ usePassword, password }) => {
    try {
        const valida = await bcrypt.compare(usePassword, password)
        return valida
    } catch (error) {
        return error
    }
}
exports.TokenJwt = async (data) => {
    if (data) {
        const token = jwt.sign(
            data,
            process.env.JWT_AUTH_KEY,
            {
                expiresIn: "1d"
            })
        return token
    }
    return null
}
exports.TokenJwtDecode = async ({ token }) => {
    const decoded = jwt.verify(token, process.env.JWT_AUTH_KEY);
    return decoded
}
exports.TokenJwtAdmin = async ({ id, email }) => {
    if (id && email) {
        const token = jwt.sign({
            id: id,
            email: email
        },
            process.env.JWT_ADM_KEY,
            {
                expiresIn: "1d"
            })
        return token
    }
    return null
}
exports.GenerateVerifyCode = async () => {
    const min = 1
    const max = 999999
    const places = 6
    const num = await (Math.floor(Math.random() * (max - min)) + min);

    const code = (num, places) => String(num).padStart(places, '0')

    const verifyCode = code(num, places)
    return verifyCode
}
