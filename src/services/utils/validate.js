const {
    formatOnlyNumbers,
    validateCpf,
    validateEmail,
    formatPhoneNumber,
    formatCpf
} = require('jgs-js-utils')

exports.isName = (name) => {
    const fullName = name.split(' ')
    if ((fullName.length >= 2) && (fullName[0].length >= 3) && (fullName[1].length >= 2)) {
        return true
    }
    return false
}

exports.isCpf = (cpf) => {
    if (validateCpf(cpf)) {
        return true
    }
    return false
}

exports.formatCpf = (document) => {
    const validate = validateCpf(document)
    if (validate) {
        const cpf = formatOnlyNumbers(document)
        return cpf
    } else {
        return false
    }
}

exports.isCelular = (phone) => {
    const celular = formatOnlyNumbers(phone)
    if (celular.length === 11) {
        return true
    }
    return false
}
exports.formatCelular = (phone) => {
    let celular = formatOnlyNumbers(phone)
    celular = formatPhoneNumber(phone)
    return celular
}

exports.isEmail = (email) => {
    if (validateEmail(email)) {
        return true
    }
    return false
}

exports.isPassword = (passwaord) => {
    return true

}
