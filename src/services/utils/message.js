const messages = require('../../assets/jsons/messages.json');
const { TodayTime  } = require('./getDateFormats')

exports.Message = async (req, res, code, data) => {
    try {
        const today = await TodayTime();
        const status = parseInt(code.substring(0,3))
        const type = parseInt(code.substring(0,1))
        let prefix

        if(type === 1){
            prefix = "WAN-"
        } else if(type === 2){
            prefix = "SUC-"
        } else if(type === 4){
            prefix = "ERR-"
        } else {
            //type = 5
            prefix = "ERR-"
        }
        const response = {
            status,
            code: `${prefix}${code}`,
            msg: messages[code],
            date: today,
            data
        }
        res.status(status).send(response)
    } catch(err){
        res.status(500).send(err)
    }
}
