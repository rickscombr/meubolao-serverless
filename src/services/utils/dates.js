const moment = require('moment')

exports.Today = (type, days) => {
    const daysAdd = days ? days : 0
    let dateTime, date, time, timestamp, timezone

    if(type === 'utc'){
        dateTime = moment().utc().add(daysAdd, 'd').format('DD/MM/YYYY HH:mm')
        date = moment().utc().add(daysAdd, 'd').format('DD/MM/YYYY')
        time = moment().utc().add(daysAdd, 'd').format('HH:mm')
        timestamp = moment().utc().add(daysAdd, 'd').unix()
        timezone = 'UTC'
    } else {
        dateTime = moment().utc().add(daysAdd, 'd').subtract(3, 'h').format('DD/MM/YYYY HH:mm')
        date = moment().utc().add(daysAdd, 'd').subtract(3, 'h').format('DD/MM/YYYY')
        time = moment().utc().add(daysAdd, 'd').subtract(3, 'h').format('HH:mm')
        timestamp = moment().utc().add(daysAdd, 'd').subtract(3, 'h').unix()
        timezone = 'GMT -3'
    }
    const today = {
        dateTime,
        date,
        time,
        timestamp,
        timezone
    }
    return today
}
exports.GameDate = ({ timestamp }) => {
    // Retorna a data e hora local no formato abaixo
    const date = moment.unix(timestamp).format('DD/MM')
    const time = moment.utc(timestamp * 1000).local().format('HH:mm')
    const gameData = { date, time }
    return gameData
}

exports.ConvertDate = (date) => {
    const newDate = moment(date).unix()
    return newDate
}
