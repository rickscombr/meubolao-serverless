const { Today } = require('../../utils/dates')
const { EncryptPassword, GenerateVerifyCode, ValidatePassword, TokenJwtAdmin } = require('../../utils/password')
const AdminMeubolao = require('../../../models/meubolao/AdminMeubolao')

exports.AmdCreateService = async ({
    name,
    document,
    email,
    phone,
    password
}) => {
    try {
        // Cria código de verificação com validade de 1 dia
        const codeVerify = await GenerateVerifyCode()
        const { timestamp: codeExpires } = await Today('local', 1)
        const polls = []
        const verified = false
        const active = true
        const cpf = String(document).replace(/\D/g, "")
        const passEncrypted = await EncryptPassword({ password })

        try {
            // Cadastra usuário aguardando verificação
            const user = await AdminMeubolao.create({
                name,
                cpf,
                email,
                phone,
                password: passEncrypted,
                polls,
                codeVerify,
                codeExpires,
                verified,
                active,
            })
            return user
        } catch (error) {
            return error
        }
    } catch (error) {
        return error
    }
}
exports.AdmFindEmailService = async ({ email }) => {
    try {
        const user = await AdminMeubolao.find({ email })
        return user
    } catch (error) {
        return error
    }
}
exports.AdmAuthenticateService = async ({ email, usePassword }) => {
    let response = {
        logged: false,
        token: null,
        id: null,
        name: null,
        email: null
    }
    try {
        const user = await this.AdmFindEmailService({ email })
        if (user.length > 0) {
            const { id, name, password } = user[0]
            const valida = await ValidatePassword({ usePassword, password })
            if (valida) {
                const token = await TokenJwtAdmin({ id, email })
                if (!!token) {
                    response = {
                        logged: true,
                        token,
                        id,
                        name,
                        email
                    }
                }
            }
        }
        return response
    } catch (error) {
        return error
    }
}
