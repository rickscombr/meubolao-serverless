const CountryMeubolao = require('../../../models/meubolao/CountryMeubolao')

exports.CountryListService = async () => {
    const countries = await CountryMeubolao.find()
    return countries
}
exports.CountryFindService = async ({ id }) => {
    const country = await CountryMeubolao.find({ _id: id })
    if (country.length > 0) {
        return country[0]
    } else {
        return {}
    }
}
exports.CountryFindCustomService = async ({ data }) => {
    const country = await CountryMeubolao.find(data)
    if (country.length > 0) {
        return country[0]
    } else {
        return {}
    }
}
exports.CountryCreateService = async ({ country }) => {
    const { name, code, flag } = country
    let newCountry
    const active = true
    const data = { name }

    const { id } = await this.CountryFindCustomService({ data })
    if (id) {
        newCountry = await CountryMeubolao.findOneAndUpdate({ _id: id },
            {
                name,
                code,
                flag,
                active,
            }
        )
    } else {
        newCountry = await CountryMeubolao.create({
            name,
            code,
            flag,
            active,
        })
    }
    return newCountry
}
