const LeagueMeubolao = require('../../../models/meubolao/LeagueMeubolao')

exports.LeagueListService = async () => {
    const leagues = await LeagueMeubolao.find()
    return leagues
}

exports.LeagueFindService = async ({ leagueId }) => {
    const league = await LeagueMeubolao.find({ leagueId })
    if (league.length > 0) {
        return league[0]
    } else {
        return {}
    }
}

exports.LeagueCreateService = async ({ countryId, league, display }) => {
    const { id: leagueId, name, type, logo } = league

    let newLeague
    const active = true

    const { id } = await this.LeagueFindService({ leagueId })
    if (id) {
        newLeague = await LeagueMeubolao.findOneAndUpdate({ leagueId },
            {
                countryId,
                name,
                display,
                type,
                logo,
                active
            }
        )
    } else {
        newLeague = await LeagueMeubolao.create({
            leagueId,
            countryId,
            name,
            display,
            type,
            logo,
            active
        })
    }
    return newLeague
}
