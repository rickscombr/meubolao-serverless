const SeasonMeubolao = require('../../../models/meubolao/SeasonMeubolao')
const { ConvertDate } = require('../../utils/dates')

exports.SeasonListService = async () => {
    const seasons = await SeasonMeubolao.find()
    return seasons
}

exports.SeasonFindService = async ({ year }) => {
    const season = await SeasonMeubolao.find({ year })
    if (season.length > 0) {
        return season[0]
    } else {
        return {}
    }
}
exports.SeasonFindCustomService = async ({ data }) => {
    const season = await SeasonMeubolao.find(data)
    if (season.length > 0) {
        return season[0]
    } else {
        return {}
    }
}

exports.SeasonCreateService = async ({ leagueId, season }) => {
    const { year, start, end, current } = season
    let newSeason
    const active = true
    const data = { leagueId, year }
    const startDate = await ConvertDate(start)
    const endDate = await ConvertDate(end)

    const { id } = await this.SeasonFindCustomService({ data })
    if (id) {
        newSeason = await SeasonMeubolao.findOneAndUpdate({ _id: id },
            {
                leagueId,
                year,
                start: startDate,
                end: endDate,
                current,
                active
            }
        )
    } else {
        newSeason = await SeasonMeubolao.create({
            leagueId,
            year,
            start: startDate,
            end: endDate,
            current,
            active
        })
    }
    return newSeason
}
