const StatisticGameBrasileirao = require('../../../models/brasileirao/StatisticGameBrasileirao')

exports.StatisticGameFindService = async ({ gameId }) => {
    const statisticGame = await StatisticGameBrasileirao.find({ gameId })
    return statisticGame
}
exports.StatisticGameCreateService = async ({ gameId, game }) => {
    const StatisticGameCheck = async ({ info }) => {
        const { gameId, teamId } = info
        let data = {}
        const existStatisticGame = await StatisticGameBrasileirao.find({ gameId, teamId })
        if (existStatisticGame.length > 0) {
            data = await StatisticGameBrasileirao.findOneAndUpdate({ gameId, teamId }, info)
        } else {
            data = await StatisticGameBrasileirao.create(info)
        }
        return data
    }

    const statisticGame = []
    let shotsGoal, shots, shotsInsideBox, shotsOutsideBox, fouls, cornerKicks, offsides, ballPossession
    let YellowCards, RedCards, goalkeeperSaves, passesTotal, passesAccurate, passesPercent

    for (let i = 0; i < game.length; i++) {
        let { team, statistics } = game[i]
        const teamId = team.id

        const shotsGoalSearch = await statistics.find(shotsGoalType => shotsGoalType.type === 'Shots on Goal')
        if (shotsGoalSearch) {
            shotsGoal = shotsGoalSearch.value === null ? 0 : shotsGoalSearch.value
        } else {
            shotsGoal = 0
        }
        const shotsSearch = await statistics.find(shotsType => shotsType.type === 'Total Shots')
        if (shotsSearch) {
            shots = shotsSearch.value === null ? 0 : shotsSearch.value
        } else {
            shots = 0
        }
        const shotsInsideBoxSearch = await statistics.find(shotsInsideBoxType => shotsInsideBoxType.type === 'Shots insidebox')
        if (shotsInsideBoxSearch) {
            shotsInsideBox = shotsInsideBoxSearch.value === null ? 0 : shotsInsideBoxSearch.value
        } else {
            shotsInsideBox = 0
        }
        const shotsOutsideBoxSearch = await statistics.find(shotsOutsideBoxType => shotsOutsideBoxType.type === 'Shots outsidebox')
        if (shotsOutsideBoxSearch) {
            shotsOutsideBox = shotsOutsideBoxSearch.value === null ? 0 : shotsOutsideBoxSearch.value
        } else {
            shotsOutsideBox = 0
        }
        const foulsSearch = await statistics.find(foulsType => foulsType.type === 'Fouls')
        if (foulsSearch) {
            fouls = foulsSearch.value === null ? 0 : foulsSearch.value
        } else {
            fouls = 0
        }
        const cornerKicksSearch = await statistics.find(cornerKicksType => cornerKicksType.type === 'Corner Kicks')
        if (cornerKicksSearch) {
            cornerKicks = cornerKicksSearch.value === null ? 0 : cornerKicksSearch.value
        } else {
            cornerKicks = 0
        }
        const offsidesSearch = await statistics.find(offsidesType => offsidesType.type === 'Offsides')
        if (offsidesSearch) {
            offsides = offsidesSearch.value === null ? 0 : offsidesSearch.value
        } else {
            offsides = 0
        }
        const ballPossessionSearch = await statistics.find(ballPossessionType => ballPossessionType.type === 'Ball Possession')
        if (ballPossessionSearch) {
            ballPossession = ballPossessionSearch.value === null ? '0%' : ballPossessionSearch.value
        } else {
            ballPossession = '0%'
        }
        const YellowCardsSearch = await statistics.find(YellowCardsType => YellowCardsType.type === 'Yellow Cards')
        if (YellowCardsSearch) {
            YellowCards = YellowCardsSearch.value === null ? 0 : YellowCardsSearch.value
        } else {
            YellowCards = 0
        }
        const RedCardsSearch = await statistics.find(RedCardsType => RedCardsType.type === 'Red Cards')
        if (RedCardsSearch) {
            RedCards = RedCardsSearch.value === null ? 0 : RedCardsSearch.value
        } else {
            RedCards = 0
        }
        const goalkeeperSavesSearch = await statistics.find(goalkeeperSavesType => goalkeeperSavesType.type === 'Goalkeeper Saves')
        if (goalkeeperSavesSearch) {
            goalkeeperSaves = goalkeeperSavesSearch.value === null ? 0 : goalkeeperSavesSearch.value
        } else {
            goalkeeperSaves = 0
        }
        const passesTotalSearch = await statistics.find(passesTotalType => passesTotalType.type === 'Total passes')
        if (passesTotalSearch) {
            passesTotal = passesTotalSearch.value === null ? 0 : passesTotalSearch.value
        } else {
            passesTotal = 0
        }
        const passesAccurateSearch = await statistics.find(passesAccurateType => passesAccurateType.type === 'Passes accurate')
        if (passesAccurateSearch) {
            passesAccurate = passesAccurateSearch.value === null ? 0 : passesAccurateSearch.value
        } else {
            passesAccurate = 0
        }
        const passesPercentSearch = await statistics.find(passesPercentType => passesPercentType.type === 'Passes %')
        if (passesPercentSearch) {
            passesPercent = passesPercentSearch.value === null ? '0%' : passesPercentSearch.value
        } else {
            passesPercent = '0%'
        }

        const info = {
            gameId,
            teamId,
            shotsGoal,
            shots,
            shotsInsideBox,
            shotsOutsideBox,
            fouls,
            cornerKicks,
            offsides,
            ballPossession,
            YellowCards,
            RedCards,
            goalkeeperSaves,
            passesTotal,
            passesAccurate,
            passesPercent,
            active: true,
        }
        const statisticGameData = await StatisticGameCheck({ info })
        statisticGame.push(statisticGameData)
    }
    return statisticGame
}
