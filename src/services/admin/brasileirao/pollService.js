const PollBrasileirao = require('../../../models/brasileirao/PollBrasileirao')

exports.PollListService = async ({ leagueId, seasonYear }) => {
    const polls = await PollBrasileirao.find({ leagueId, seasonYear })
    return polls
}

exports.PollFindService = async ({ leagueId, seasonYear, key }) => {
    const poll = await PollBrasileirao.find({ leagueId, seasonYear, key })
    if (poll.length > 0) {
        return poll[0]
    } else {
        return null
    }
}

exports.PollCreateService = async ({ leagueId, seasonYear, key, name, start, end, current, active }) => {
    const poll = await PollBrasileirao.create({ leagueId, seasonYear, key, name, start, end, current, active })
    return poll
}
