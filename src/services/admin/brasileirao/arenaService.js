const ArenaBrasileirao = require('../../../models/brasileirao/ArenaBrasileirao')
const { CountryFindCustomService } = require('../meubolao/countryService')
const { FootArenaIdService } = require('../footballService')
const ArenaNames = require('../../../assets/json/arenas.json')

exports.ArenaCheck = async ({ arenaId, teamId }) => {
    const respArena = await ArenaBrasileirao.find({ arenaId })
    if (respArena.length > 0) {
        return respArena[0]
    } else {
        const {
            name,
            address,
            city,
            country,
            capacity,
            image
        } = await FootArenaIdService({ arenaId })

        const data = { name: country }
        const { id: countryId } = await CountryFindCustomService({ data })

        let display = ArenaNames[name]
        if (!display) {
            display = name
        }
        const arena = {
            arenaId,
            teamId,
            countryId,
            name,
            display,
            address,
            city,
            capacity,
            image,
            active: true
        }
        const newArena = await ArenaBrasileirao.create(arena)
        return newArena
    }
}
