const GunnerBrasileirao = require('../../../models/brasileirao/GunnerBrasileirao')

exports.GunnerListService = async ({ leagueId, seasonYear }) => {
    const gunner = await GunnerBrasileirao.find({ leagueId, seasonYear }).sort({ goals: -1 })
    return gunner
}

exports.GunnerCreateService = async ({ leagueId, seasonYear, gunners }) => {
    const newGunners = []
    for (let i = 0; i < gunners.length; i++) {
        const {
            player: {
                id: playerId,
                name,
                age,
                nationality,
                photo,
            },
            statistics
        } = gunners[i]

        const {
            team: {
                id: teamId,
                name: teamName,
                logo: teamLogo,
            },
            games: {
                appearences: games
            },
            shots: {
                total: shots
            },
            goals: {
                total: goals,
                conceded: goalsConceded,
                assists: goalsAssists,
                saves: goalsSaves,
            }
        } = statistics[0]

        const qtdGames = !games ? 0 : games
        const qtdShots = !shots ? 0 : shots
        const qtdGoals = !goals ? 0 : goals
        const qtdGoalsConceded = !goalsConceded ? 0 : goalsConceded
        const qtdGoalsAssists = !goalsAssists ? 0 : goalsAssists
        const qtdGoalsSaves = !goalsSaves ? 0 : goalsSaves

        const gunner = {
            leagueId,
            seasonYear,
            teamId,
            teamName,
            teamLogo,
            playerId,
            name,
            age,
            nationality,
            photo,
            games: qtdGames,
            shots: qtdShots,
            goals: qtdGoals,
            goalsConceded: qtdGoalsConceded,
            goalsAssists: qtdGoalsAssists,
            goalsSaves: qtdGoalsSaves,
            active: true,
        }

        //Procura artilheiro, se existe atualiza, se não cadastra
        let newGunner
        const exists = await GunnerBrasileirao.find({ playerId }) || []
        if (exists.length > 0) {
            newGunner = await GunnerBrasileirao.findOneAndUpdate({ playerId }, gunner)
        } else {
            newGunner = await GunnerBrasileirao.create(gunner)
        }
        newGunners.push(newGunner)
    }
    console.log('PASSO 2.17 ==>', newGunners)
    return newGunners
}
