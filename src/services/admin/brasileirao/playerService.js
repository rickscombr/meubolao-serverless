const PlayerBrasileirao = require('../../../models/brasileirao/PlayerBrasileirao')

exports.PlayerListService = async ({ teamId }) => {
    const players = await PlayerBrasileirao.find({ teamId })
    return players
}

exports.PlayerFindService = async ({ playerId }) => {
    const player = await PlayerBrasileirao.find({ playerId })
    if (player.length > 0) {
        return player[0]
    } else {
        return {}
    }
}

exports.PlayerCreateService = async ({ data }) => {
    const { team: { id: teamId }, players } = data
    let player = {}, newPlayers = []

    for (let p = 0; p < players.length; p++) {
        player = {
            playerId: players[p].id,
            teamId,
            name: players[p].name,
            age: players[p].age,
            number: players[p].number,
            position: players[p].position,
            photo: players[p].photo,
            active: true,
        }
        const newPlayer = await PlayerBrasileirao.create(player)

        newPlayers.push(newPlayer)
    }
    return newPlayers
}

exports.PlayersRemoveService = async ({ teamId }) => {
    const players = await PlayerBrasileirao.deleteMany({ teamId })
    return players
}
