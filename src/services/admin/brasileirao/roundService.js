const RoundBrasileirao = require('../../../models/brasileirao/RoundBrasileirao')

exports.RoundListService = async ({ leagueId, seasonYear }) => {
    const round = await RoundBrasileirao.find({ leagueId, seasonYear }).sort({ round: 1 })
    return round
}
exports.RoundFindService = async ({ leagueId, seasonYear }) => {
    const round = await RoundBrasileirao.find({ leagueId, seasonYear, current: true })
    return round
}

exports.RoundCreateService = async ({ leagueId, seasonYear, rounds, current }) => {
    //Percorre todos os Rounds e insere um a um não sendo o atual
    for (let i = 0; i < rounds.length; i++) {
        let roundNumber = rounds[i].match(/\d/g).join("")
        const round = {
            leagueId,
            seasonYear,
            name: `Rodada ${roundNumber}`,
            originalName: rounds[i],
            round: roundNumber,
            current: false,
            active: true,
        }
        const newRound = await RoundBrasileirao.create(round)
    }
    //Identifica o Round atual e muda para current: true
    let currentNumber = current[0].match(/\d/g).join("")
    await RoundBrasileirao.findOneAndUpdate({ leagueId, seasonYear, round: currentNumber }, { current: true })
    const allRounds = await RoundBrasileirao.find({ leagueId, seasonYear }).sort({ round: 1 })
    return allRounds
}

exports.RoundUpdateService = async ({ leagueId, seasonYear, current }) => {
    // Atualiza todos as rodadas para não corrente
    await RoundBrasileirao.updateMany({ leagueId, seasonYear }, { "$set": { current: false } })
    //Identifica o Round atual e muda para current: true
    let currentNumber = current[0].match(/\d/g).join("")
    await RoundBrasileirao.findOneAndUpdate({ leagueId, seasonYear, round: currentNumber }, { current: true })
    const rounds = await RoundBrasileirao.find({ leagueId, seasonYear }).sort({ round: 1 })
    return rounds
}

exports.RoundRemoveService = async ({ leagueId, seasonYear }) => {
    const round = await RoundBrasileirao.deleteMany({ leagueId, seasonYear })
    return round
}

exports.RoundCheck = async ({ leagueId, seasonYear, round }) => {
    const rounds = await RoundBrasileirao.find({ leagueId, seasonYear, round })
    const { id } = rounds[0]
    return id
}
