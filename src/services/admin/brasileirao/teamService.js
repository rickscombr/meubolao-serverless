const TeamBrasileirao = require('../../../models/brasileirao/TeamBrasileirao')
const ArenaBrasileirao = require('../../../models/brasileirao/ArenaBrasileirao')
const ArenaNames = require('../../../assets/json/arenas.json')

exports.TeamListService = async () => {
    const teams = await TeamBrasileirao.find()
    return teams
}

exports.TeamFindService = async ({ teamId }) => {
    const team = await TeamBrasileirao.find({ teamId })
    if (team.length > 0) {
        return team[0]
    } else {
        return {}
    }
}

exports.TeamCreateService = async ({ data, countryId }) => {
    let team = {}, teams = [], newTeam = {}
    let arena = {}, arenas = [], newArena = {}

    for (let i = 0; i < data.length; i++) {
        const teamId = data[i].team.id
        const arenaId = data[i].venue.id
        team = {
            teamId,
            countryId,
            arenaId: data[i].venue.id,
            name: data[i].team.name,
            code: data[i].team.code,
            founded: data[i].team.founded,
            national: data[i].team.national,
            logo: data[i].team.logo,
            active: true,
        }
        arena = {
            arenaId: arenaId,
            teamId,
            countryId,
            name: data[i].venue.name,
            display: ArenaNames[data[i].venue.name] || 'undefined',
            address: data[i].venue.address,
            city: data[i].venue.city,
            capacity: data[i].venue.capacity,
            image: data[i].venue.image,
            active: true
        }

        const respTeam = await TeamBrasileirao.find({ teamId })
        if (respTeam.length > 0) {
            newTeam = await TeamBrasileirao.findOneAndUpdate({ teamId }, team)
        } else {
            newTeam = await TeamBrasileirao.create(team)
        }
        teams.push(newTeam)

        const respArena = await ArenaBrasileirao.find({ arenaId })
        if (respArena.length > 0) {
            newArena = await ArenaBrasileirao.findOneAndUpdate({ arenaId }, arena)
        } else {
            newArena = await ArenaBrasileirao.create(arena)
        }
        arenas.push(newArena)
    }

    // retorna array de times para cadatrar os Players
    let players = []
    for (let p = 0; p < teams.length; p++) {
        const teamId = teams[p].teamId
        players.push({ teamId })
    }

    return { teams, arenas, players }
}
