const RankingBrasileirao = require('../../../models/brasileirao/RankingBrasileirao')

exports.RankingListService = async ({ leagueId, seasonYear }) => {
    const ranking = await RankingBrasileirao.find({ leagueId, seasonYear }).sort({ rank: 1 })
    console.log('RESPONSE DO SERVICE ====================>', ranking)
    return ranking
}

exports.RankingCreateService = async ({ leagueId, seasonYear, data }) => {
    let ranking = []
    for (let i = 0; i < data.length; i++) {
        const description = data[i].description
        let tag = 'NCL'
        if (description === 'CONMEBOL Libertadores') {
            tag = 'LIB'
        }
        if (description === 'CONMEBOL Libertadores Qualifiers') {
            tag = 'QLB'
        }
        if (description === 'CONMEBOL Sudamericana') {
            tag = 'SUL'
        }
        if (description === 'CONMEBOL Sudamericana Group Stage') {
            tag = 'QSL'
        }
        if (description === 'Relegation') {
            tag = 'RBX'
        }
        const teamRank = {
            leagueId,
            seasonYear,
            teamId: data[i].team.id,
            rank: data[i].rank,
            group: data[i].group,
            lastRank: data[i].status,
            points: data[i].points,
            games: data[i].all.played,
            win: data[i].all.win,
            draw: data[i].all.draw,
            lose: data[i].all.lose,
            goalsFor: data[i].all.goals.for,
            goalsAgainst: data[i].all.goals.against,
            goalsDiff: data[i].goalsDiff,
            tag: tag,
            active: true,
        }
        const rank = await RankingBrasileirao.create(teamRank)
        ranking.push(rank)
    }
    return ranking
}

exports.RankingRemoveService = async ({ leagueId, seasonYear }) => {
    const ranking = await RankingBrasileirao.deleteMany({ leagueId, seasonYear })
    return ranking
}
