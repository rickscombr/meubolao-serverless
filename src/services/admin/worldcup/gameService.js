const GameWorldcup = require('../../../models/worldcup/GameWorldcup')
const { RoundCheck } = require('./roundService')
const { TeamFindService } = require('./teamService')

exports.GameListService = async ({ leagueId, seasonYear }) => {
    const game = await GameWorldcup.find({ leagueId, seasonYear }).sort({ date: 1 })
    return game
}
exports.GameFindService = async ({ gameId }) => {
    const game = await GameWorldcup.find({ gameId })
    return game[0]
}
exports.GameCreateService = async ({ leagueId, seasonYear, games }) => {
    const checkGame = async ({ game }) => {
        const { gameId } = game
        const respGame = await GameWorldcup.find({ gameId })
        let newGame = {}
        if (respGame.length > 0) {
            newGame = await GameWorldcup.findOneAndUpdate({ gameId }, game)
        } else {
            newGame = await GameWorldcup.create(game)
        }
        return newGame
    }

    //Percorre todos os Games e insere um a um não sendo o atual
    let arenaId, respGames = []

    for (let i = 0; i < games.length; i++) {
        const name = games[i].league.round
        const round = name.match(/\d/g).join("")
        const roundId = await RoundCheck({ leagueId, seasonYear, round })

        const game = {
            leagueId,
            seasonYear,
            gameId: games[i].fixture.id,
            roundId,
            masterId: games[i].teams.home.id,
            visitorId: games[i].teams.away.id,
            arenaName: games[i].fixture.venue.name,
            arenaCity: games[i].fixture.venue.city,
            date: games[i].fixture.timestamp,
            timezone: games[i].fixture.timezone,
            masterGoals: games[i].goals.home ? games[i].goals.home : 0,
            visitorGoals: games[i].goals.away ? games[i].goals.away : 0,
            winner: games[i].teams.home.winner === true ? 1 : games[i].teams.away.winner === true ? 2 : 0,
            status: games[i].fixture.status.short,
            active: true,
        }

        const actualGame = await checkGame({ game })
        respGames.push(actualGame)
    }
    return respGames
}
// exports.GameUpdateService = async ({ leagueId, seasonYear, current }) => {
//     // Atualiza todos as rodadas para não corrente
//     await GameWorldcup.findOneAndUpdate({ leagueId, seasonYear }, { current: false })

//     //Identifica o Game atual e muda para current: true
//     let currentNumber = current[0].match(/\d/g).join("")
//     await GameWorldcup.findOneAndUpdate({ leagueId, seasonYear, game: currentNumber }, { current: true })
//     const games = await GameWorldcup.find({ leagueId, seasonYear }).sort({ game: 1 })
//     return games
// }
exports.GameRemoveService = async ({ leagueId, seasonYear }) => {
    const game = await GameWorldcup.deleteMany({ leagueId, seasonYear })
    return game
}
