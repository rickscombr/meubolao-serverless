const TeamWorldcup = require('../../../models/worldcup/TeamWorldcup')

exports.TeamListService = async () => {
    const teams = await TeamWorldcup.find()
    return teams
}

exports.TeamFindService = async ({ teamId }) => {
    const team = await TeamWorldcup.find({ teamId })
    if (team.length > 0) {
        return team[0]
    } else {
        return {}
    }
}

exports.TeamCreateService = async ({ data, countryId }) => {
    let team = {}, teams = [], newTeam = {}

    for (let i = 0; i < data.length; i++) {
        const teamId = data[i].team.id
        const name = data[i].team.name
        const nameCode = `${name.toUpperCase().replace(/(A|E|I|O|U|)/gi, '').replace(/\s/g, '')}XXX`
        const code = data[i].team.code
            ? data[i].team.code
            : nameCode.substring(0, 3)

        team = {
            teamId,
            countryId,
            name,
            code,
            founded: data[i].team.founded,
            national: data[i].team.national,
            logo: data[i].team.logo,
            active: true,
        }

        const respTeam = await TeamWorldcup.find({ teamId })
        if (respTeam.length > 0) {
            newTeam = await TeamWorldcup.findOneAndUpdate({ teamId }, team)
        } else {
            newTeam = await TeamWorldcup.create(team)
        }
        teams.push(newTeam)
    }

    // retorna array de times para cadatrar os Players
    let players = []
    for (let p = 0; p < teams.length; p++) {
        const teamId = teams[p].teamId
        players.push({ teamId })
    }

    return { teams, players }
}
