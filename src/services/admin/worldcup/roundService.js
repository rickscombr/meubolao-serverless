const RoundWorldcup = require('../../../models/worldcup/RoundWorldcup')

exports.RoundListService = async ({ leagueId, seasonYear }) => {
    const round = await RoundWorldcup.find({ leagueId, seasonYear }).sort({ round: 1 })
    return round
}
exports.RoundFindService = async ({ leagueId, seasonYear }) => {
    const round = await RoundWorldcup.find({ leagueId, seasonYear, current: true })
    return round
}

exports.RoundCreateService = async ({ leagueId, seasonYear, rounds, current }) => {
    //Percorre todos os Rounds e insere um a um não sendo o atual
    for (let i = 0; i < rounds.length; i++) {
        let roundNumber = rounds[i].match(/\d/g).join("")
        const round = {
            leagueId,
            seasonYear,
            name: `Rodada ${roundNumber}`,
            originalName: rounds[i],
            round: roundNumber,
            current: false,
            active: true,
        }
        const newRound = await RoundWorldcup.create(round)
    }
    //Identifica o Round atual e muda para current: true
    let currentNumber = current[0].match(/\d/g).join("")
    await RoundWorldcup.findOneAndUpdate({ leagueId, seasonYear, round: currentNumber }, { current: true })
    const allRounds = await RoundWorldcup.find({ leagueId, seasonYear }).sort({ round: 1 })
    return allRounds
}

exports.RoundUpdateService = async ({ leagueId, seasonYear, current }) => {
    // Atualiza todos as rodadas para não corrente
    await RoundWorldcup.updateMany({ leagueId, seasonYear }, { "$set": { current: false } })
    //Identifica o Round atual e muda para current: true
    let currentNumber = current[0].match(/\d/g).join("")
    await RoundWorldcup.findOneAndUpdate({ leagueId, seasonYear, round: currentNumber }, { current: true })
    const rounds = await RoundWorldcup.find({ leagueId, seasonYear }).sort({ round: 1 })
    return rounds
}

exports.RoundRemoveService = async ({ leagueId, seasonYear }) => {
    const round = await RoundWorldcup.deleteMany({ leagueId, seasonYear })
    return round
}

exports.RoundCheck = async ({ leagueId, seasonYear, round }) => {
    const rounds = await RoundWorldcup.find({ leagueId, seasonYear, round })
    const { id } = rounds[0]
    return id
}
