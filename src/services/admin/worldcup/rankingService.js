const RankingWorldcup = require('../../../models/worldcup/RankingWorldcup')

exports.RankingListService = async ({ leagueId, seasonYear }) => {
    const ranking = await RankingWorldcup.find({ leagueId, seasonYear }).sort({ rank: 1 })
    return ranking
}

exports.RankingCreateService = async ({ leagueId, seasonYear, data }) => {
    let ranking = []
    for (let i = 0; i < data.length; i++) {
        const teamRank = {
            leagueId,
            seasonYear,
            teamId: data[i].team.id,
            rank: data[i].rank,
            group: data[i].group,
            lastRank: data[i].status,
            points: data[i].points,
            games: data[i].all.played,
            win: data[i].all.win,
            draw: data[i].all.draw,
            lose: data[i].all.lose,
            goalsFor: data[i].all.goals.for,
            goalsAgainst: data[i].all.goals.against,
            goalsDiff: data[i].goalsDiff,
            active: true,
        }
        const rank = await RankingWorldcup.create(teamRank)
        ranking.push(rank)
    }
    return ranking
}

exports.RankingRemoveService = async ({ leagueId, seasonYear }) => {
    const ranking = await RankingWorldcup.deleteMany({ leagueId, seasonYear })
    return ranking
}
