const axios = require('axios')

exports.FootLeagueService = async ({ leagueId, year }) => {
    const urlLeague = `https://${process.env.FOOTBALL_HOST}/leagues?id=${leagueId}&season=${year}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlLeague, options)
    return response
}
exports.FootTeamService = async ({ leagueId, year }) => {
    const urlTeam = `https://${process.env.FOOTBALL_HOST}/teams?league=${leagueId}&season=${year}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlTeam, options)
    return response
}
exports.FootPlayerService = async ({ teamId }) => {
    const urlPlayer = `https://${process.env.FOOTBALL_HOST}/players/squads?team=${teamId}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlPlayer, options)
    return response[0]
}
exports.FootRankingService = async ({ leagueId, seasonYear }) => {
    const urlRanking = `https://${process.env.FOOTBALL_HOST}/standings?league=${leagueId}&season=${seasonYear}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlRanking, options)
    const { league: { standings } } = response[0]
    const ranking = standings[0]
    return ranking
}
exports.FootRoundService = async ({ leagueId, seasonYear: season, actual }) => {
    const current = actual ? `&current=true` : ``
    const urlRound = `https://${process.env.FOOTBALL_HOST}/fixtures/rounds?league=${leagueId}&season=${season}${current}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlRound, options)
    return response
}
exports.FootGameService = async ({ leagueId, seasonYear, round }) => {
    const urlGame = `https://${process.env.FOOTBALL_HOST}/fixtures?league=${leagueId}&season=${seasonYear}&round=${round}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlGame, options)
    return response
}
exports.FootArenaIdService = async ({ arenaId }) => {
    const urlArena = `https://${process.env.FOOTBALL_HOST}/venues?id=${arenaId}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlArena, options)
    const {
        name,
        address,
        city,
        country,
        capacity,
        image
    } = response[0]
    const arena = {
        name,
        address,
        city,
        country,
        capacity,
        image
    }
    return arena
}
exports.FootStatisticGameService = async ({ gameId }) => {
    const urlStatistic = `https://${process.env.FOOTBALL_HOST}/fixtures/statistics?fixture=${gameId}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlStatistic, options)
    return response
}

exports.FootGunnerService = async ({ leagueId, seasonYear }) => {
    const urlTeam = `https://${process.env.FOOTBALL_HOST}/players/topscorers?league=${leagueId}&season=${seasonYear}`
    const options = {
        headers: {
            'x-rapidapi-key': `${process.env.FOOTBALL_API_KEY}`,
            'x-rapidapi-host': `${process.env.FOOTBALL_HOST}`
        }
    }
    const { data: { response } } = await axios.get(urlTeam, options)
    console.log('GUNNERS ===>', response)
    return response
}
