const RankingBrasileirao = require('../../models/brasileirao/RankingBrasileirao')
const TeamBrasileirao = require('../../models/brasileirao/TeamBrasileirao')

exports.RankingListService = async ({ leagueId, seasonYear }) => {
    const ranking = []
    const rankingData = await RankingBrasileirao.find({ leagueId, seasonYear, active: true }).sort({ rank: 1 })
    for (let i = 0; i < rankingData.length; i++) {
        const {
            id,
            leagueId,
            seasonYear,
            group,
            teamId,
            rank,
            games,
            points,
            win,
            draw,
            lose,
            goalsFor,
            goalsAgainst,
            goalsDiff,
            lastRank,
            tag,
            active,
        } = rankingData[i]

        const performance = parseInt(((win * 3 + draw) / (games * 3)) * 100, 10)
        const {
            name: teamName,
            code: teamCode,
            logo: teamLogo,
        } = await TeamBrasileirao.findOne({ teamId })

        const newRanking = {
            id,
            leagueId,
            seasonYear,
            group,
            teamId,
            teamName,
            teamCode,
            teamLogo,
            rank,
            games,
            points,
            win,
            draw,
            lose,
            goalsFor,
            goalsAgainst,
            goalsDiff,
            lastRank,
            performance,
            tag,
            active,
        }
        ranking.push(newRanking)
    }
    return ranking
}
