const GameBrasileirao = require('../../models/brasileirao/GameBrasileirao')
const RoundBrasileirao = require('../../models/brasileirao/RoundBrasileirao')
const TeamBrasileirao = require('../../models/brasileirao/TeamBrasileirao')
const ArenaBrasileirao = require('../../models/brasileirao/ArenaBrasileirao')
const { GameDate } = require('../../services/utils/dates')

exports.GameLastNextService = async ({ status, order, limit }) => {
    let games = []
    const gamesData = await GameBrasileirao.find({ status }).sort({ date: order }).limit(limit)
    for (let i = 0; i < gamesData.length; i++) {
        const {
            id,
            leagueId,
            seasonYear,
            gameId,
            roundId,
            masterId,
            visitorId,
            arenaId,
            date,
            timezone,
            masterGoals,
            visitorGoals,
            winner,
            status,
            active,
        } = gamesData[i]

        // Converte timestamp em data e hora do jogo
        const { date: gameDate, time: gameTime } = await GameDate({ timestamp: date })

        // Recupera o nome da rodada e monta o objeto
        const { name: roundName } = await RoundBrasileirao.findOne({ _id: roundId })
        const round = {
            roundId,
            name: roundName
        }

        // Recupera os dados do mandante e monta o objeto
        const {
            name: masterName,
            code: masterCode,
            logo: masterLogo,
        } = await TeamBrasileirao.findOne({ teamId: masterId })
        const master = {
            masterId,
            name: masterName,
            code: masterCode,
            goals: masterGoals,
            logo: masterLogo,
        }
        // Recupera os dados do visitante e monta o objeto
        const {
            name: visitorName,
            code: visitorCode,
            logo: visitorLogo,
        } = await TeamBrasileirao.findOne({ teamId: visitorId })
        const visitor = {
            visitorId,
            name: visitorName,
            code: visitorCode,
            goals: visitorGoals,
            logo: visitorLogo,
        }
        // Recupera os dados do estádio e monta o objeto
        const {
            name: arenaName,
            city: arenaCity,
        } = await ArenaBrasileirao.findOne({ arenaId })
        const arena = {
            arenaId,
            name: arenaName,
            city: arenaCity,
        }
        // Monta o objeto de response do jogo
        const newGame = {
            id,
            leagueId,
            seasonYear,
            gameId,
            round,
            master,
            visitor,
            arena,
            date,
            gameDate,
            gameTime,
            timezone,
            winner,
            status,
            active,
        }
        games.push(newGame)
    }
    return games
}
exports.GameListService = async ({ leagueId, seasonYear, roundId }) => {
    let games = []
    const gamesData = await GameBrasileirao.find({ leagueId, seasonYear, roundId }).sort({ date: 1 })
    for (let i = 0; i < gamesData.length; i++) {
        const {
            id,
            leagueId,
            seasonYear,
            gameId,
            roundId,
            masterId,
            visitorId,
            arenaId,
            date,
            timezone,
            masterGoals,
            visitorGoals,
            winner,
            status,
            active,
        } = gamesData[i]

        // Converte timestamp em data e hora do jogo
        const { date: gameDate, time: gameTime } = await GameDate({ timestamp: date })

        // Recupera o nome da rodada e monta o objeto
        const { name: roundName } = await RoundBrasileirao.findOne({ _id: roundId })
        const round = {
            roundId,
            name: roundName
        }

        // Recupera os dados do mandante e monta o objeto
        const {
            name: masterName,
            code: masterCode,
            logo: masterLogo,
        } = await TeamBrasileirao.findOne({ teamId: masterId })
        const master = {
            masterId,
            name: masterName,
            code: masterCode,
            goals: masterGoals,
            logo: masterLogo,
        }
        // Recupera os dados do visitante e monta o objeto
        const {
            name: visitorName,
            code: visitorCode,
            logo: visitorLogo,
        } = await TeamBrasileirao.findOne({ teamId: visitorId })
        const visitor = {
            visitorId,
            name: visitorName,
            code: visitorCode,
            goals: visitorGoals,
            logo: visitorLogo,
        }
        // Recupera os dados do estádio e monta o objeto
        const {
            name: arenaName,
            city: arenaCity,
        } = await ArenaBrasileirao.findOne({ arenaId })
        const arena = {
            arenaId,
            name: arenaName,
            city: arenaCity,
        }
        // Monta o objeto de response do jogo
        const newGame = {
            id,
            leagueId,
            seasonYear,
            gameId,
            round,
            master,
            visitor,
            arena,
            date,
            gameDate,
            gameTime,
            timezone,
            winner,
            status,
            active,
        }
        games.push(newGame)
    }
    return games
}
