const RoundBrasileirao = require('../../models/brasileirao/RoundBrasileirao')

exports.RoundListService = async ({ leagueId, seasonYear }) => {
    const round = await RoundBrasileirao.find({ leagueId, seasonYear }).sort({ round: 1 })
    return round
}
