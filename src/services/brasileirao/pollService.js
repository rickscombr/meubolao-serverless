const PollBrasileirao = require('../../models/brasileirao/PollBrasileirao')

exports.PollFindService = async ({ leagueId, seasonYear, key }) => {
    const poll = await PollBrasileirao.find({ leagueId, seasonYear, key })
    if (poll.length > 0) {
        return poll[0]
    } else {
        return null
    }
}
