const GunnerBrasileirao = require('../../models/brasileirao/GunnerBrasileirao')

exports.GunnerListService = async ({ leagueId, seasonYear }) => {
    const gunners = await GunnerBrasileirao.find({ leagueId, seasonYear }).sort({ goals: -1, games: 1 })
    return gunners
}
